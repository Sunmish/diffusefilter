#! /usr/bin/env python

"""
Pipeline for EMU extra-galactic field filtering.

Python implementation of the diffuse image filtering technique described
by L. Rudnick (2002):
https://ui.adsabs.harvard.edu/abs/2002NewAR..46..101R/abstract
with a few extra bells and whistles.
"""

from filter import pipeline
from argparse import ArgumentParser, RawTextHelpFormatter

import logging
logging.basicConfig(format="%(levelname)s: (EMU) %(message)s")
logger = logging.getLogger("filter.logger")
logger.setLevel(logging.INFO)


def get_args():
    ps = ArgumentParser(
        description=__doc__,
        formatter_class=RawTextHelpFormatter)
    
    ps.add_argument("image",
        type=str,
        help="The image to filter."
    )

    ps.add_argument("-S", "-o", "--suffix", "--outbase",
        type=str,
        default="",
        dest="suffix",
        help="Suffix to append to output image names. Default is to append only "
             "\".diffuse.fits\" to the input image name. If the output image "
             "exists it WILL be overwritten."
    )

    ps.add_argument("-O", "--output_all",
        action="store_true",
        help="Output all intermediate images. " 
             "A default naming scheme will be used along with --suffix."
    )

    ps.add_argument("-d", "--debug",
        action="store_true",
        help="Enable debug output."
    )
    
    ps.add_argument("-c", "--clip", 
        default=4,
        type=float,
        help="Sigma clip level: negative values below the sigma clipping "
             "thresholds will be clipped prior to filtering. "
             "Sensible values range from 2-5 sigma. "
             "Default 4."
    )

    ps.add_argument("-m", "--max_size",
        default=-1,
        type=int,
        help="Maximum image size for convolution before breaking it into "
             "chunks to reduce RAM footprint. "
             "If working on a high-memory machine (>=64 GB), this number can "
             " be arbitrarily high. Default -1 (no chunks)."
        )
    
    ps.add_argument("--no_parallel",
        action="store_false",
        dest="parallel",
        help="Disable parallelisation of the convolving routine. "
             "All cores are normally used, but disable if encountering issues."
    )

    args = ps.parse_args()
    
    return args


def cli(args):
     
    if args.debug:
        logger.setLevel(logging.DEBUG)
    
    pipeline.emu_extragalactic_filter(
        image=args.image,
        output_all=args.output_all,
        suffix=args.suffix,
        sigma_clip_level=args.clip,
        max_size=args.max_size,
        parallel=args.parallel
    )


if __name__ == "__main__":
     cli(get_args())