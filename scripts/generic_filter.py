#! /usr/bin/env python

"""
Pipeline for generic filtering.

Python implementation of the diffuse image filtering technique described
by L. Rudnick (2002):
https://ui.adsabs.harvard.edu/abs/2002NewAR..46..101R/abstract
with a few extra bells and whistles.
"""

from filter import pipeline
from argparse import ArgumentParser, RawTextHelpFormatter

import logging
logging.basicConfig(format="%(levelname)s: %(message)s")
logger = logging.getLogger("filter.logger")
logger.setLevel(logging.INFO)

def get_args():
    ps = ArgumentParser(
        description=__doc__,
        formatter_class=RawTextHelpFormatter)
    ps.add_argument("image")
    ps.add_argument("-s", "--scale",
        default=None,
        nargs="*",
        type=float,
        help="Scale for diffuse filtering in arcsec. Default None "
             "(use --filter_factor instead)."
    )
    ps.add_argument("-f", "--filter_factor",
        default=[3.],
        type=float,
        nargs="*",
        help="Factor times the PSF major axis to filter emission. If multiple "
             "factors are supplied, multiple rounds of filtering are done. "
             "Default [3]. Not used if --scale is set."
    )
    ps.add_argument("-c", "--clip_level",
        type=float,
        default=-1,
        help="Map values below -1*clip_level will be clipped. "
             "Default is to clip based on 1*rms.")
    ps.add_argument("-R", "--use_rms_for_clip",
        action="store_true",
        help="Use the map rms for calculating the clip level. The clip level "
             "is then clip_level*rms."
    )
    ps.add_argument("-r", "--replace_clip",
        action="store_true",
        help="Replace clipped pixels with clip value."
    )
    ps.add_argument("-M", "--max_size",
        default=1024,
        type=int,
        help="Maximum image size for convolution before breaking it into "
             "chunks (saves RAM!). Default 1024."
    )
    ps.add_argument("-C", "--convolve_setting",
        type=str,
        default="final",
        choices=["final", "after", "before"],
        help="Convolving option. Either as a final step (\"final\"), "
             "after ALL filtering (\"after\"), "
             "or before EACH subsequent filter (\"before\")."
    )
    ps.add_argument("-b", "--beam_scale",
        type=float,
        nargs="*",
        default=None,
        help="Smooth output data to this resolution (arcsec). Default is to "
             "use the input filtering scales depending on --convolve_setting."
    )
    ps.add_argument("-B", "--beam_filter_factors",
        type=float,
        nargs="*",
        default=None,
        help=""
    )
    ps.add_argument("-FB", "--beam_final",
        type=float,
        default=None,
        help=""
    )

    ps.add_argument("-S", "-o", "--suffix", "--outbase",
        type=str,
        default="",
        dest="suffix",
        help="Suffix to append to output image names. Default is to append only "
             "\".diffuse.fits\" to the input image name. If the output image "
             "exists it WILL be overwritten."
    )
    ps.add_argument("-O", "--output_all",
        action="store_true",
        help="Output all intermediate images. " 
             "A default naming scheme will be used."
    )
    ps.add_argument("-d", "--debug",
        action="store_true",
        help="Enable debug statements.")
    args = ps.parse_args()
    return args


def cli(args):
     
     if args.debug:
         logger.setLevel(logging.DEBUG)
     
     pipeline.generic_filter(
         image=args.image,
         scale=args.scale,
         filter_factor=args.filter_factor,
         max_size=args.max_size,
         convolve_setting=args.convolve_setting,
         convolving_beams=args.beam_scale,
         suffix=args.suffix,
         output_all=args.output_all,
         clip_level=args.clip_level,
         use_rms_for_clip=args.use_rms_for_clip,
         replace_clip=args.replace_clip
     )


if __name__ == "__main__":
     cli(get_args())