#! /usr/bin/env python

import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

reqs = [
    "astropy",
    "numpy",
    "scipy",
    "tqdm",
    "radio-beam"
]

scripts = [
    "./scripts/generic_filter.py",
    "./scripts/EMU_filter.py",
]



setuptools.setup(
    name="DiffuseFilter",
    version="2.2.0",
    author="SWD",
    author_email="stefanduchesne@gmail.com",
    description="Filter out emission of a given scale via the minimum/maxmimum filtering method of Rudnick (2002).",
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=reqs,
    packages=["filter"],
    scripts=scripts
)
