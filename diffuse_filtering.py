#! /usr/bin/env python


"""
Python implementation of the diffuse image filtering technique described
by L. Rudnick (2002):
https://ui.adsabs.harvard.edu/abs/2002NewAR..46..101R/abstract

Implemented in python by S. Duchesne.
Modifications by L. Rudnick. Default options:
Open map 3x beam - open map 27xbeam, zero, convolve 3xbeam.

Options allow general modification of the above procedure. 
"""


from argparse import ArgumentParser, RawTextHelpFormatter
import warnings
from astropy.io import fits
from astropy import units as u
from astropy.convolution import convolve_fft, Gaussian2DKernel
from astropy.stats import sigma_clip
import numpy as np
from scipy import ndimage


import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

try:
    from tqdm.contrib import itertools
except ImportError:
    logger.warning("No tqdm package, will use normal itertools (noprogress bars!)")
    import itertools

from radio_beam import Beam
from radio_beam.utils import BeamError


def get_args():
    ps = ArgumentParser(
        description=__doc__,
        formatter_class=RawTextHelpFormatter)
    ps.add_argument("image")
    ps.add_argument("-s", "--scale",
        default=None,
        nargs="*",
        type=float,
        help="Scale for diffuse filtering in arcsec. Default None "
             "(use --filter_factor instead)."
    )
    ps.add_argument("-f", "--filter_factor",
        default=[3., 27.],
        type=float,
        nargs="*",
        help="Factor times the PSF major axis to filter emission. If multiple "
             "factors are supplied, multiple rounds of filtering are done. "
             "Default [3, 27]. Not used if --scale is set."
    )
    ps.add_argument("-c", "--clip_level",
        type=float,
        default=-1,
        help="Map values below -1*clip_level will be clipped. "
             "Default is to clip based on 1*rms.")
    ps.add_argument("-R", "--use_rms_for_clip",
        action="store_true",
        help="Use the map rms for calculating the clip level. The clip level "
             "is then clip_level*rms."
    )
    ps.add_argument("-r", "--replace_clip",
        action="store_true",
        help="Replace clipped pixels with clip value."
    )
    ps.add_argument("-M", "--max_size",
        default=1024,
        type=int,
        help="Maximum image size for convolution before breaking it into "
             "chunks (saves RAM!). Default 1024."
    )
    ps.add_argument("-C", "--convolve_setting",
        type=str,
        choices=["final", "after", "before"],
        help="Convolving option. Either as a final step (\"final\"), "
             "after ALL filtering (\"after\"), "
             "or before EACH subsequent filter (\"before\")."
    )
    ps.add_argument("-b", "--beam_scale",
        type=float,
        nargs="*",
        default=None,
        help="Smooth output data to this resolution (arcsec). Default is to "
             "use the input filtering scales depending on --convolve_setting."
    )
    ps.add_argument("-B", "--beam_filter_factors",
        type=float,
        nargs="*",
        default=None,
        help=""
    )
    ps.add_argument("-FB", "--beam_final",
        type=float,
        default=None,
        help=""
    )

    ps.add_argument("-S", "-o", "--suffix", "--outbase",
        type=str,
        default="",
        dest="suffix",
        help="Suffix to append to output image names. Default is to append only "
             "\".diffuse.fits\" to the input image name. If the output image "
             "exists it WILL be overwritten."
    )
    ps.add_argument("-O", "--output_all",
        action="store_true",
        help="Output all intermediate images. " 
             "A default naming scheme will be used."
    )
    ps.add_argument("-d", "--debug",
        action="store_true",
        help="Enable debug statements.")
    args = ps.parse_args()
    return args


def get_beam(header):
    """Wrapper for obtaining beam information from radio-beam."""
    beam = Beam.from_fits_header(header)
    return beam

def max_filter(array, size):
    """Wrapper for scipy's maximum filter.
    
    Include NaN filtering: https://stackoverflow.com/a/52927788
    
    """
    idx = np.where(np.isnan(array))
    array_temp = array.copy()
    array_temp[idx] = -np.inf
    output_array = ndimage.maximum_filter(array_temp, size=size)
    return output_array

def min_filter(array, size):
    """Wrapper for scipy's minimum filter."""
    idx = np.where(np.isnan(array))
    array_temp = array.copy()
    array_temp[idx] = np.inf
    output_array = ndimage.minimum_filter(array_temp, size=size)
    return output_array


def fwhm_to_sigma(fwhm):
    """Convert FWHM to sigma.

    Args:
        fwhm: FWHM.

    Returns:
        Sigma from the given FWHM in the same units.

    """

    return fwhm/np.sqrt(8.*np.log(2.))


def strip_wcsaxes(hdr):
    """Strip extra axes in a header object. Also retain frequency information.

    Args:
        hdr: astropy.io.fits.Header object.

    Returns
        Header with non-celestial axes removed.

    """

    freq = None
    if "FREQ" in hdr.keys():
        try:
            # TODO: find out why we have this try-except.
            # Likely some non-standard FREQ we encountered?
            float(hdr["FREQ"])
            freq = hdr["FREQ"]
        except Exception:
            pass
    for i in [3, 4]:
        if "CRVAL{}".format(i) in hdr.keys() and freq is None:
            if "FREQ" in hdr["CTYPE{}".format(i)]:
                freq = hdr["CRVAL{}".format(i)]
            if "SPECLNMF" in hdr["CTYPE{}".format(i)]:
                freq = hdr["CRVAL{}".format(i)]

    if "RESTFREQ" in hdr.keys() and freq is None:
        freq = hdr["RESTFREQ"]

    if freq is None:
        logger.warning("No frequency information found!")
    else:
        hdr["FREQ"] = freq

    remove_keys = [key+i for key in
        ["CRVAL", "CDELT", "CRPIX", "CTYPE", "CUNIT", "NAXIS"]
        for i in ["3", "4", "5"]
    ]

    for key in remove_keys:
        if key in hdr.keys():
            del hdr[key]

    return hdr


def get_last2d(array):
    """Get the celestial axes of an array to ensure it is 2-dimensional.

    From https://stackoverflow.com/a/27111239

    Args:
        array: Image data from a FITS image.

    Returns:
        2-D array.

    """

    if array.ndim <= 2:
        return array
    else:
        slc = [0] * (array.ndim - 2)
        slc += [slice(None), slice(None)]
        return array[tuple(slc)]


def reset_zero_level(array, f=np.nanmean, clip=False):
    """Reset the zero level of the open map.

    Args:
        array: The open map as an array.

    Returns:
        The zeroed open map.

    """

    if clip:
        mean = f(
            sigma_clip(
                np.squeeze(array), axis=(0, 1), masked=False
            )
        )
    else:
        mean = f(array[np.isfinite(array)])
    logger.debug("Zero level: {} Jy/beam".format(mean))
    array -= mean

    return array


def map_rms(array):
    """Get overall rms of the map.
    
    Args:
        array: The map as an array.

    Returns:
        Map rms.

    """

    perc_nan1 = len(array[np.where(np.isnan(array))].flatten())
    carray = sigma_clip(np.squeeze(array), axis=(0, 1), masked=False)
    perc_nan2 = len(array[np.where(np.isnan(carray))].flatten())
    clipped_pixel_perc = (perc_nan2 - perc_nan1) / (array.shape[-2]*array.shape[-1])
    logger.debug("{:.5f}% pixels clipped.".format(clipped_pixel_perc*100.))
    rms = np.sqrt(np.nanmean(carray**2))
    logger.debug("Map RMS: {} Jy/beam".format(rms))
    
    return rms, carray


def clip_below(array, clip_value, replace=False, fill_value=np.nan):
    """Clip map values below clip value.

    Args:
        array: The map as an array.
        clip_value: The value below which to clip. This sets values to NaN.
    
    Returns:
        The clipped map.
    
    """

    if replace:
        array[array < clip_value] = clip_value
    else:
        array[array < clip_value] = fill_value

    return array


def sigma_clip_below(array, sigma, replace=False, fill_value=np.nan):
    """Sigma clip map values below sigma value.
    
    Args:
        array: The map as an array.
        sigma: 
    """

    perc_nan1 = len(array[np.where(np.isnan(array))].flatten())

    carray = sigma_clip(np.squeeze(array), 
        axis=(0, 1), 
        sigma_lower=sigma,
        sigma_upper=1e30,
        masked=False
    )

    perc_nan2 = len(array[np.where(np.isnan(carray))].flatten())
    clipped_pixel_perc = (perc_nan2 - perc_nan1) / (array.shape[-2]*array.shape[-1])
    logger.debug("{:.5f}% pixels clipped.".format(clipped_pixel_perc*100.))
    
    carray[np.isnan(carray)] = fill_value

    return carray


def convolve(arr, pixscale, major, minor, pa,
    maxsize=1024,
    border=72):
    """Convolve an array.

    Args:
        arr:
            Array to convolve.
        pixscale:
            Angular size of the pixels.
        major:
            Major axis for the convolving kernel.
        minor:
            Minor axis for the convolving kernel.
        pa:
            Position angle for the convolving kernel.
        maxsize:
            Maximum subarray size for convolution. Convolving a whole image 
            using `convolve_fft` is memory intensive, so chunking is typically
            done. If `maxsize` is greater than the image size,
            the whole image is convolved at once. Default 1024 pixels.
        border:
            Border to pad convolution for subarrays. Avoids aliasing
            problems. Default 100 pixels.

    Returns:
    Convolved array.

    """

    sigma_x = fwhm_to_sigma(major/pixscale)
    sigma_y = fwhm_to_sigma(minor/pixscale)
    theta = np.radians(pa)

    kern = Gaussian2DKernel(sigma_x, sigma_y, theta)

    # Edge effects occur if the boundaries are too close to the beam size - 
    # extra padding is somewhat arbitrary at the moment, but a factor of 5 
    # seems to work well.
    if maxsize < sigma_x*5 or maxsize < sigma_y*5:
        maxsize = int(max([sigma_x, sigma_y])*2)
        logger.warning(
            "Max chunk size smaller than convolving beam - "
            " increasing max chunk size to {}".format(maxsize)
        )
    if border < sigma_x*5 or maxsize < sigma_y*5:
        border = int(max([sigma_x, sigma_y])*5)
        logger.warning(
            "Convolution border size is smaller than convolving beam - "
            " increasing border size to {}".format(border)
        )

    if border > maxsize:
        logger.info("Setting max chunk size to match border size.")
        maxsize = border
    


    if np.max(arr.shape) > maxsize:
            # Convolve in chunks across the image to preserve memory:
            # Determine number of subarrays:
        logger.warning(
            "Array larger than max convolution size - processing array in chunks."
        )
        nx = arr.shape[-2] // maxsize
        ny = arr.shape[-1] // maxsize
        rx = arr.shape[-2] % maxsize
        ry = arr.shape[-1] % maxsize

        if rx > 0:
            nx += 1
        if ry > 0:
            ny += 1

        garr = np.squeeze(arr)
        conv_arr = np.full_like(garr, np.nan)
        garr_indices = np.indices(garr.shape)

        n = 0
        x_range = range(0, garr.shape[-2], maxsize)
        y_range = range(0, garr.shape[-1], maxsize)

        for i, j in itertools.product(x_range, y_range):
            subx = garr_indices[0][i:i+maxsize, j:j+maxsize]
            suby = garr_indices[1][i:i+maxsize, j:j+maxsize]

            if i-border < 0:
                i_min_border = 0
            else:
                i_min_border = border
            if i+maxsize+border > garr.shape[-2]-1:
                i_max_border = 0
            else:
                i_max_border = border
            if j-border < 0:
                j_min_border = 0
            else:
                j_min_border = border
            if j+maxsize+border > garr.shape[-1]-1:
                j_max_border = 0
            else:
                j_max_border = border

            sub_garr = garr[
                i-i_min_border:i+maxsize+i_max_border, 
                j-j_min_border:j+maxsize+j_max_border
            ]
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                conv = convolve_fft(sub_garr, kern,
                    allow_huge=True,
                    boundary="fill",
                    fill_value=np.nan,
                    normalize_kernel=True,
                    preserve_nan=True
                )

            conv = conv[
                i_min_border:sub_garr.shape[-2]-i_max_border, 
                j_min_border:sub_garr.shape[-1]-j_max_border
            ]
            conv_arr[subx, suby]  = conv

            n += 1

    else:

        conv_arr = convolve_fft(
            arr,
            kern,
            preserve_nan=True,
            fill_value=0.,
            allow_huge=True,
            normalize_kernel=True,
        )

    return conv_arr


def filter_image(image, 
    scale=None, 
    filter_factor=[3.],
    suffix="", 
    max_size=1024,
    convolve_setting="final",
    convolving_beams=None,
    output_all=False,
    clip_level=-1,
    use_rms_for_clip=False,
    replace_clip=False):
    """Filter an image.

    Args:
        image:
            FITS image to filter.
        scale:
            Angular scale in arcsec to filter. Leave unset to filter based 
            on the PSF size.
        filter_factor:
            If `scale` is unset, then the angular filter is 
            `filter_factor`*BMAJ. Default 3.
        suffix:
            Suffix to append to output images. Default is to append 
            \"${suffix}.diffuse.fits\" to the input image name. 
            If the output images exists they WILL be overwritten. 
        max_size:
            Maximum subarray size for convolution. Convolving a whole image 
            using `convolve_fft` is memory intensive, so chunking is 
            typically done. If `max_size` is greater than the image size,
            the whole image is convolved at once. Default 1024.
        convolve_setting:
            Convolving option. Either as a final step (\"final\"), "
            after ALL filtering (\"after\"),
            or before EACH subsequent filter (\"before\"). Default \"final\".
        convolving_beam:
            Smooth output data to this resolution (arcsec). 
            Default is to use the input filtering scales depending 
            on `convolve_setting`."
        output_all:
            Switch to output all intermediate image files with default names.
        clip_level:
        use_rms_for_clip:
        replace_clip:

    TODO: convolve filtered map with original beam as well.

    """

    convolve_setting = convolve_setting.lower()
    if convolve_setting not in ["final", "after", "before"]:
        raise RuntimeError(
            "Convolve setting must be one of \"final\", \"after\", \"before\"." \
            " {} not a valid convolving option.".format(convolve_setting)
        )
    
    hdu = fits.open(image)
    data = get_last2d(hdu[0].data)
    hdr = strip_wcsaxes(hdu[0].header)
    cd = hdr["CDELT2"]

    beam = get_beam(hdr)
    logger.info("Beam: {:.1f}\" * {:.1f}\", {:.1f} deg".format(
        beam.major.value*3600., beam.minor.value*3600., beam.pa.value
    ))

    if scale is None:
        scale = [beam.major.value * ff for ff in filter_factor]
    else:
        scale = [s/3600. for s in scale]

    if convolving_beams is not None:
        if len(convolving_beams) != len(scale):
            logger.warning("Setting a single convolving beam for all scales.")
            cbeam = [convolving_beams[0]/3600.]*len(scale)
        else:
            cbeam = [c/3600. for c in convolving_beams]
        
    else:
        cbeam = scale

    pixel_scale = [int(s / cd) for s  in scale]
    pixel_str = ", ".join(
        ["[{}] {:.1f}\" ({} pixels)".format(
            i+1, scale[i]*3600., pixel_scale[i] 
        ) for i in range(len(scale))]
    )
    logger.info("Filter scale(s): {}".format(pixel_str))

    if clip_level >= 0.:
       
        rms, carray = map_rms(data)
        if output_all:
            clipped_outname = image.replace(".fits", "") + "{}.sclip.fits".format(
                    suffix
            )
            fits.writeto(
                clipped_outname, carray, hdr, overwrite=True
            )
        if use_rms_for_clip:
            clip_level = rms*clip_level
        logger.debug("Clipping map below {}".format(-clip_level))
        data = clip_below(data, -clip_level, replace=replace_clip)

    e_filtered_maps = []
    d_filtered_maps = []
    e_filtered_convolved = []
    d_filtered_convolved = []
    for i in range(len(scale)):

        logger.debug("Running minimum filter {}".format(i+1))
        e_filtered = min_filter(data, pixel_scale[i])
        logger.debug("Running maximum filter {}".format(i+1))
        d_filtered = max_filter(e_filtered, pixel_scale[i])

        if output_all:
            fits.writeto(
                image.replace(".fits", "") + "{}.filter{}.fits".format(
                    suffix, i+1
                ),
                e_filtered, hdr,
                overwrite=True
            )
            fits.writeto(
                image.replace(".fits", "") + "{}.open{}.fits".format(
                    suffix, i+1
                ),
                d_filtered, hdr,
                overwrite=True
            )

        if convolve_setting == "before":
           
            logger.debug("Reseting zero level in open map {}".format(i+1))
            d_filtered = reset_zero_level(d_filtered, clip=True)

            logger.debug("Convolving open map {}".format(i+1))
            final_beam = Beam(
                    major=cbeam[i]*u.deg, 
                    minor=cbeam[i]*u.deg, 
                    pa=0.*u.deg
            )

            try:
                convolving_beam = final_beam.deconvolve(beam)
            except BeamError:
                convolving_beam = final_beam


            logger.debug(
                "Convolving beam {3}: {0:.1f}\" * {1:.1f}\", {2:.1f} deg".format(
                    convolving_beam.major.value*3600.,
                    convolving_beam.minor.value*3600., 
                    convolving_beam.pa.value,
                    i+1
            ))

            d_filtered_c = convolve(
                arr=d_filtered,
                pixscale=cd,
                major=convolving_beam.major.value,
                minor=convolving_beam.minor.value,
                pa=convolving_beam.pa.value,
                maxsize=max_size
            )

            if output_all:
                fits.writeto(
                    image.replace(".fits", "") + "{}.smooth{}.fits".format(
                        suffix, i+1
                    ),
                    d_filtered_c, hdr,
                    overwrite=True
                )

        else:
            d_filtered_c = d_filtered
                    
        e_filtered_maps.append(e_filtered)
        d_filtered_maps.append(d_filtered)
        d_filtered_convolved.append(d_filtered_c)
        data = d_filtered_c

    combined_filtered = None
    for i in range(0, len(scale)):

        if convolve_setting == "after":

            # logger.debug("Resetting zero level in map {}".format(i+1))
            # d_filtered_maps[i] = reset_zero_level(d_filtered_maps[i])

            logger.debug("Convolving open map {}".format(i+1))
            final_beam = Beam(
                    major=cbeam[i]*u.deg, 
                    minor=cbeam[i]*u.deg, 
                    pa=0.*u.deg
            )
            try:
                convolving_beam = final_beam.deconvolve(beam)
            except BeamError:
                convolving_beam = final_beam

            logger.debug(
                "Convolving beam {3}: {0:.1f}\" * {1:.1f}\", {2:.1f} deg".format(
                    convolving_beam.major.value*3600.,
                    convolving_beam.minor.value*3600., 
                    convolving_beam.pa.value,
                    i+1
            ))

            d_filtered_maps[i] = convolve(
                arr=d_filtered_maps[i],
                pixscale=cd,
                major=convolving_beam.major.value,
                minor=convolving_beam.minor.value,
                pa=convolving_beam.pa.value,
                maxsize=max_size
            )

            if output_all:
                fits.writeto(
                    image.replace(".fits", "") + "{}.smooth{}.fits".format(
                        suffix, i+1
                    ),
                    d_filtered_maps[i], hdr,
                    overwrite=True

                )

        else:
            d_filtered_maps[i] = d_filtered_convolved[i]

        if combined_filtered is None:
            combined_filtered = d_filtered_maps[i]
        else:
            logger.debug("Subtracting filter map {}".format(i+1))
            combined_filtered -= d_filtered_maps[i]

    # logger.debug("Resetting zero level in final open map")
    # combined_filtered = reset_zero_level(combined_filtered)

    # convolve_setting = "final"
    # cbeam[0] = cbeam[-1]
    if convolve_setting == "final":

        logger.debug("Resetting zero level in final open map")
        combined_filtered = reset_zero_level(combined_filtered)

        final_beam = Beam(
            major=cbeam[0]*u.deg, 
            minor=cbeam[0]*u.deg, 
            pa=0.*u.deg
        )
        try:
            convolving_beam = final_beam.deconvolve(beam)
        except BeamError:
            convolving_beam = final_beam

        logger.debug(
            "Convolving beam {3}: {0:.1f}\" * {1:.1f}\", {2:.1f} deg".format(
                convolving_beam.major.value*3600.,
                convolving_beam.minor.value*3600., 
                convolving_beam.pa.value,
                i+1
        ))

        logger.debug("Convolving final open map")
        logger.debug(
            "Convolving beam: {0:.1f}\" * {1:.1f}\", {2:.1f} deg".format(
                convolving_beam.major.value*3600.,
                convolving_beam.minor.value*3600., 
                convolving_beam.pa.value
        ))

        combined_filtered = convolve(
            arr=combined_filtered,
            pixscale=cd,
            major=convolving_beam.major.value,
            minor=convolving_beam.minor.value,
            pa=convolving_beam.pa.value,
            maxsize=max_size
        )

    fits.writeto(
        image.replace(".fits", "") + "{}.diffuse.fits".format(suffix), 
        combined_filtered, 
        hdr, 
        overwrite=True
    )

    logger.debug("All done!")


def cli(args):
     
     if args.debug:
         logger.setLevel(logging.DEBUG)
     
     filter_image(
         image=args.image,
         scale=args.scale,
         filter_factor=args.filter_factor,
         max_size=args.max_size,
         convolve_setting=args.convolve_setting,
        #  convolving_beams=args.beams,
         suffix=args.suffix,
         output_all=args.output_all,
         clip_level=args.clip_level,
         use_rms_for_clip=args.use_rms_for_clip,
         replace_clip=args.replace_clip
     )


if __name__ == "__main__":
     cli(get_args())