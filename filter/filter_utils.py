#! /usr/bin/env python

import warnings

from astropy.io import fits
from astropy.stats import sigma_clip
import numpy as np
from scipy import ndimage

import logging
logger = logging.getLogger("filter.logger")


def strip_wcsaxes(hdr):
    """Strip extra axes in a header object. Also retain frequency information.

    Args:
        hdr: astropy.io.fits.Header object.

    Returns
        Header with non-celestial axes removed.

    """

    freq = None
    if "FREQ" in hdr.keys():
        try:
            # TODO: find out why we have this try-except.
            # Likely some non-standard FREQ we encountered?
            float(hdr["FREQ"])
            freq = hdr["FREQ"]
        except Exception:
            pass
    for i in [3, 4]:
        if "CRVAL{}".format(i) in hdr.keys() and freq is None:
            if "FREQ" in hdr["CTYPE{}".format(i)]:
                freq = hdr["CRVAL{}".format(i)]
            if "SPECLNMF" in hdr["CTYPE{}".format(i)]:
                freq = hdr["CRVAL{}".format(i)]

    if "RESTFREQ" in hdr.keys() and freq is None:
        freq = hdr["RESTFREQ"]

    if freq is None:
        logger.warning("No frequency information found!")
    else:
        hdr["FREQ"] = freq

    remove_keys = [key+i for key in
        ["CRVAL", "CDELT", "CRPIX", "CTYPE", "CUNIT", "NAXIS"]
        for i in ["3", "4", "5"]
    ]

    for key in remove_keys:
        if key in hdr.keys():
            del hdr[key]

    return hdr


def get_last2d(array):
    """Get the celestial axes of an array to ensure it is 2-dimensional.

    From https://stackoverflow.com/a/27111239

    Args:
        array: Image data from a FITS image.

    Returns:
        2-D array.

    """

    if array.ndim <= 2:
        return array
    else:
        slc = [0] * (array.ndim - 2)
        slc += [slice(None), slice(None)]
        return array[tuple(slc)]


def assign_data(hdu, array):
    """Assign output cube correctly.

    Args:
        hdu: astropy.io.fits.HDUList object.
        array: The array to assign to the celestial hdu[0].data dims.
    
    Returns:
        The HDUList object with the new celestial data.

    """

    if hdu[0].data.ndim == 2:
        hdu[0].data = array
    else:
        hdu[0].data[..., :, :] = array

    return hdu[0].data


def reset_zero_level(array, f=np.nanmean, clip=False):
    """Reset the zero level of the open map.

    Args:
        array: The open map as an array.

    Returns:
        The zeroed open map.

    """

    if clip:
        mean = f(
            sigma_clip(
                np.squeeze(array), axis=(0, 1), masked=False
            )
        )
    else:
        mean = f(array[np.isfinite(array)])
    logger.debug("Zero level: {} Jy/beam".format(mean))
    array -= mean

    return array


def map_rms(array):
    """Get overall rms of the map.
    
    Args:
        array: The map as an array.

    Returns:
        Map rms.

    """

    perc_nan1 = len(array[np.where(np.isnan(array))].flatten())
    carray = sigma_clip(np.squeeze(array), axis=(0, 1), masked=False)
    perc_nan2 = len(array[np.where(np.isnan(carray))].flatten())
    clipped_pixel_perc = (perc_nan2 - perc_nan1) / (array.shape[-2]*array.shape[-1])
    logger.debug("{:.5f}% pixels clipped.".format(clipped_pixel_perc*100.))
    rms = np.sqrt(np.nanmean(carray**2))
    logger.debug("Map RMS: {} Jy/beam".format(rms))
    
    return rms, carray


def clip_below(array, clip_value, replace=False, fill_value=np.nan):
    """Clip map values below clip value.

    Args:
        array: The map as an array.
        clip_value: The value below which to clip. This sets values to NaN.
    
    Returns:
        The clipped map.
    
    """

    if replace:
        array[array < clip_value] = clip_value
    else:
        array[array < clip_value] = fill_value

    return array


def sigma_clip_below(array, sigma, fill_value=np.nan):
    """Sigma clip map values below sigma value.
    
    Args:
        array: np.ndarray 
            The map as an array.
        sigma: float
            The number of standard deviations that pixels <0 are clipped.
        fill_value: float or np.nan
            Replace NaN values with this fill value.

    Returns:
        The clipped map.
    """

    perc_nan1 = len(array[np.where(np.isnan(array))].flatten())

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        s_array = sigma_clip(np.squeeze(array), 
            axis=(0, 1), 
            sigma=sigma,
            masked=False
        )

    array[array < 0] = s_array[array < 0]

    perc_nan2 = len(array[np.where(np.isnan(array))].flatten())
    clipped_pixel_perc = (perc_nan2 - perc_nan1) / (array.shape[-2]*array.shape[-1])
    logger.debug("{1:.5f}% pixels {0} sigma clipped.".format(
        sigma, clipped_pixel_perc*100.
    ))
    
    array[np.isnan(array)] = fill_value

    return array


def max_filter(array, size):
    """Wrapper for scipy's maximum filter.
    
    Includes NaN filtering: https://stackoverflow.com/a/52927788
    
    """
    idx = np.where(np.isnan(array))
    array_temp = array.copy()
    array_temp[idx] = -np.inf
    output_array = ndimage.maximum_filter(array_temp, size=size)
    return output_array


def min_filter(array, size):
    """Wrapper for scipy's minimum filter."""
    idx = np.where(np.isnan(array))
    array_temp = array.copy()
    array_temp[idx] = np.inf
    output_array = ndimage.minimum_filter(array_temp, size=size)
    return output_array


def writeout_compact_map(data, open_map, hdu, outname):
    """Wrapper for writing out the compact/residual maps after filtering.

    Args:
        data: np.ndarray
            The data array of the original pre-filtered data.
        open_map: np.ndarray
            The open map to be subtracted from the data.
        header: astropy.io.fits.Header
            The FITS header object associated with the data.
        outname: str
            Output FITS image name.

    """

    compact_map = data - open_map

    hdu[0].data = assign_data(hdu, compact_map)
    fits.writeto(outname, hdu[0].data, hdu[0].header, overwrite=True)