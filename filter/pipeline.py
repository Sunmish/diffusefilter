#! /usr/bin/env python

import numpy as np

from astropy.io import fits
from astropy import units as u

from .beam_utils import (
    get_beam_from_header,
    get_beam, 
    deconvolve_beam,
    convolve
)

from .filter_utils import (
    strip_wcsaxes,
    get_last2d,
    reset_zero_level,
    map_rms,
    clip_below,
    sigma_clip_below,
    max_filter,
    min_filter,
    writeout_compact_map,
    assign_data
)

import logging
# logging.basicConfig(format="%(levelname)s (%(function)s): %(message)s")
logger = logging.getLogger("filter.logger")
# logger.setLevel(logging.INFO)


def generic_filter(image,
    scale=None,
    filter_factor=[3.],
    suffix="",
    max_size=1024,
    convolve_setting="final",
    convolving_beams=None,
    output_all=False,
    clip_level=-1,
    use_rms_for_clip=False,
    replace_clip=False):
    """Filter an image with generic options.

    TODO: all

    Args:
        image:
            FITS image to filter.
        scale:
            Angular scale in arcsec to filter. Leave unset to filter based 
            on the PSF size.
        filter_factor:
            If `scale` is unset, then the angular filter is 
            `filter_factor`*BMAJ. Default 3.
        suffix:
            Suffix to append to output images. Default is to append 
            \"${suffix}.diffuse.fits\" to the input image name. 
            If the output images exists they WILL be overwritten. 
        max_size:
            Maximum subarray size for convolution. Convolving a whole image 
            using `convolve_fft` is memory intensive, so chunking is 
            typically done. If `max_size` is greater than the image size,
            the whole image is convolved at once. Default 1024.
        convolve_setting:
            Convolving option. Either as a final step (\"final\"), "
            after ALL filtering (\"after\"),
            or before EACH subsequent filter (\"before\"). Default \"final\".
        convolving_beam:
            Smooth output data to this resolution (arcsec). 
            Default is to use the input filtering scales depending 
            on `convolve_setting`."
        output_all:
            Switch to output all intermediate image files with default names.
        clip_level:
        use_rms_for_clip:
        replace_clip:

    TODO: convolve filtered map with original beam as well.

    """

    convolve_setting = convolve_setting.lower()
    if convolve_setting not in ["final", "after", "before"]:
        raise RuntimeError(
            "Convolve setting must be one of \"final\", \"after\", \"before\"." \
            " {} not a valid convolving option.".format(convolve_setting)
        )
    
    hdu = fits.open(image)
    data = get_last2d(hdu[0].data)
    hdr = strip_wcsaxes(hdu[0].header)
    cd = hdr["CDELT2"]

    beam = get_beam_from_header(hdr)

    if scale is None:
        scale = [beam.major.value * ff for ff in filter_factor]
    else:
        scale = [s/3600. for s in scale]

    if convolving_beams is not None:
        if len(convolving_beams) != len(scale):
            logger.warning("Setting a single convolving beam for all scales.")
            cbeam = [convolving_beams[0]/3600.]*len(scale)
        else:
            cbeam = [c/3600. for c in convolving_beams]
        
    else:
        cbeam = scale

    pixel_scale = [int(s / cd) for s  in scale]
    pixel_str = ", ".join(
        ["[{}] {:.1f}\" ({} pixels)".format(
            i+1, scale[i]*3600., pixel_scale[i] 
        ) for i in range(len(scale))]
    )
    logger.info("Filter scale(s): {}".format(pixel_str))

    if clip_level >= 0.:
       
        # rms, carray = map_rms(data)
        # if output_all:
        #     clipped_outname = image.replace(".fits", "") + "{}.sclip.fits".format(
        #             suffix
        #     )
        #     fits.writeto(
        #         clipped_outname, carray, hdr, overwrite=True
        #     )
        # if use_rms_for_clip:
        #     clip_level = rms*clip_level
        logger.debug("Clipping map below {}".format(-clip_level))
        # data = clip_below(data, -clip_level, replace=replace_clip)
        data = sigma_clip_below(data, clip_level)
        if output_all:
            clipped_outname = image.replace(".fits", "") + "{}.sclip.fits".format(
                    suffix
            )
            fits.writeto(
                clipped_outname, data, hdr, overwrite=True
            )

    e_filtered_maps = []
    d_filtered_maps = []
    e_filtered_convolved = []
    d_filtered_convolved = []
    for i in range(len(scale)):

        logger.debug("Running minimum filter {}".format(i+1))
        e_filtered = min_filter(data, pixel_scale[i])
        logger.debug("Running maximum filter {}".format(i+1))
        d_filtered = max_filter(e_filtered, pixel_scale[i])

        if output_all:
            fits.writeto(
                image.replace(".fits", "") + "{}.filter{}.fits".format(
                    suffix, i+1
                ),
                e_filtered, hdr,
                overwrite=True
            )
            fits.writeto(
                image.replace(".fits", "") + "{}.open{}.fits".format(
                    suffix, i+1
                ),
                d_filtered, hdr,
                overwrite=True
            )

        if convolve_setting == "before":
           
            logger.debug("Reseting zero level in open map {}".format(i+1))
            d_filtered = reset_zero_level(d_filtered, clip=True)

            logger.debug("Convolving open map {}".format(i+1))

            final_beam = get_beam(cbeam[i], cbeam[i], 0)
            convolving_beam = deconvolve_beam(final_beam, beam)

            d_filtered_c = convolve(
                arr=d_filtered,
                pixscale=cd,
                major=convolving_beam.major.value,
                minor=convolving_beam.minor.value,
                pa=convolving_beam.pa.value,
                maxsize=max_size
            )

            if output_all:
                fits.writeto(
                    image.replace(".fits", "") + "{}.smooth{}.fits".format(
                        suffix, i+1
                    ),
                    d_filtered_c, hdr,
                    overwrite=True
                )

        else:
            d_filtered_c = d_filtered
                    
        e_filtered_maps.append(e_filtered)
        d_filtered_maps.append(d_filtered)
        d_filtered_convolved.append(d_filtered_c)
        data = d_filtered_c

    combined_filtered = None
    for i in range(0, len(scale)):

        if convolve_setting == "after":

            # logger.debug("Resetting zero level in map {}".format(i+1))
            # d_filtered_maps[i] = reset_zero_level(d_filtered_maps[i])

            logger.debug("Convolving open map {}".format(i+1))
            final_beam = get_beam(cbeam[i], cbeam[i], 0)
            convolving_beam = deconvolve_beam(final_beam, beam)

            logger.debug(
                "Convolving beam {3}: {0:.1f}\" * {1:.1f}\", {2:.1f} deg".format(
                    convolving_beam.major.value*3600.,
                    convolving_beam.minor.value*3600., 
                    convolving_beam.pa.value,
                    i+1
            ))

            d_filtered_maps[i] = convolve(
                arr=d_filtered_maps[i],
                pixscale=cd,
                major=convolving_beam.major.value,
                minor=convolving_beam.minor.value,
                pa=convolving_beam.pa.value,
                maxsize=max_size
            )

            if output_all:
                fits.writeto(
                    image.replace(".fits", "") + "{}.smooth{}.fits".format(
                        suffix, i+1
                    ),
                    d_filtered_maps[i], hdr,
                    overwrite=True
                )

        else:
            d_filtered_maps[i] = d_filtered_convolved[i]

        if combined_filtered is None:
            combined_filtered = d_filtered_maps[i]
        else:
            logger.debug("Subtracting filter map {}".format(i+1))
            combined_filtered -= d_filtered_maps[i]

    # logger.debug("Resetting zero level in final open map")
    # combined_filtered = reset_zero_level(combined_filtered)

    # convolve_setting = "final"
    # cbeam[0] = cbeam[-1]
    if convolve_setting == "final":

        logger.debug("Resetting zero level in final open map")
        combined_filtered = reset_zero_level(combined_filtered, clip=True)


        final_beam = get_beam(cbeam[0], cbeam[0], 0)
        convolving_beam = deconvolve_beam(final_beam, beam)

        logger.debug(
            "Convolving beam {3}: {0:.1f}\" * {1:.1f}\", {2:.1f} deg".format(
                convolving_beam.major.value*3600.,
                convolving_beam.minor.value*3600., 
                convolving_beam.pa.value,
                i+1
        ))

        logger.debug("Convolving final open map")
        logger.debug(
            "Convolving beam: {0:.1f}\" * {1:.1f}\", {2:.1f} deg".format(
                convolving_beam.major.value*3600.,
                convolving_beam.minor.value*3600., 
                convolving_beam.pa.value
        ))

        combined_filtered = convolve(
            arr=combined_filtered,
            pixscale=cd,
            major=convolving_beam.major.value,
            minor=convolving_beam.minor.value,
            pa=convolving_beam.pa.value,
            maxsize=max_size
        )

    fits.writeto(
        image.replace(".fits", "") + "{}.diffuse.fits".format(suffix), 
        combined_filtered, 
        hdr, 
        overwrite=True
    )

    logger.debug("All done!")




def emu_extragalactic_filter(image,
    filter_factors=[3, 27],
    suffix="",
    output_all=False,
    sigma_clip_level=4,
    max_size=-1,
    parallel=True):
    """
    Filter optimized specifically for EMU extra-galactic fields.
    
    Args:
        image: str
            FITS image name to filter.
        filter_factors: list, optional
            The angular filters [min, max] are `filter_factor`*BMAJ. 
            Default [3, 27].
        suffix: str, optional
            Suffix to append to output images. Default is to append 
            \"${suffix}.diffuse.fits\" to the input image name. 
            If the output images exists they WILL be overwritten. 
        output_all: bool, optional
            Switch to output all intermediate image files with default names.
        sigma_clip_level: float, optional
            Level to sigma clip data prior to filtering. Note can result
            in NaN propagating through the filtering, which may indicate the 
            scales are not present. Default 4 (sigma).
        max_size: int, optional
            Maximum subarray size for convolution. Convolving a whole image 
            using `convolve_fft` is memory intensive, so chunking can be done. 
            If `max_size` is greater than the image size or <0,
            the whole image is convolved at once. Default -1.
        parallel: bool, optional
            Enable parallelisation during convolution. Default on.

    """

    hdu = fits.open(image)
    data = get_last2d(hdu[0].data)
    # hdr = strip_wcsaxes(hdu[0].header)
    hdr = hdu[0].header
    cd = hdr["CDELT2"]

    beam = get_beam_from_header(hdr)
    scales = [beam.major.value * ff for ff in filter_factors]
    convolving_beams = scales  # 

    pixel_scales = [int(s/cd) for s in scales]
    pixel_str = ", ".join(
        ["[{}] {:.1f}\" ({} pixels)".format(
            i+1, scales[i]*3600., pixel_scales[i] 
        ) for i in range(len(scales))]
    )
    logger.info("Filter scale(s): {}".format(pixel_str))

    # Optionally sigma-clip the NEGATIVE pixels to remove some artefacts
    # that can introduce further artefacts during clipping. 
    if sigma_clip_level is not None:
        data = sigma_clip_below(data, sigma=sigma_clip_level)
        if output_all:
            hdu[0].data = assign_data(hdu, data)
            fits.writeto(
                image.replace(".fits", "") + "{}.sclip.fits".format(suffix),
                hdu[0].data, hdr,
                overwrite=True
            )

    d_convolved_maps = []

    for i in range(len(scales)):

        logger.debug("Minimum filter at scale {:.1f}\"".format(scales[i]*3600.))
        e_filtered = min_filter(data, pixel_scales[i])
        
        logger.debug("Maximum filter at scale {:.1f}\"".format(scales[i]*3600.))
        d_filtered = max_filter(e_filtered, pixel_scales[i])

        if output_all:
            hdu[0].data = assign_data(hdu, e_filtered)
            fits.writeto(
                image.replace(".fits", "") + "{}.filter{}.fits".format(
                    suffix, i+1
                ),
                hdu[0].data, hdr,
                overwrite=True
            )
            hdu[0].data = assign_data(hdu, d_filtered)
            fits.writeto(
                image.replace(".fits", "") + "{}.open{}.fits".format(
                    suffix, i+1
                ),
                hdu[0].data, hdr,
                overwrite=True
            )

        logger.debug("Smoothing open map to {:.1f}\"".format(scales[i]*3600.))
        
        final_beam = get_beam(convolving_beams[i])  # assume circular
        convolving_beam = deconvolve_beam(final_beam, beam)

        d_filtered_c = convolve(
            arr=d_filtered,
            pixscale=cd,
            major=convolving_beam.major.value,
            minor=convolving_beam.minor.value,
            pa=convolving_beam.pa.value,
            maxsize=max_size,
            parallel=parallel
        )

        if output_all:
            hdu[0].data = assign_data(hdu, d_filtered_c)
            fits.writeto(
                image.replace(".fits", "") + "{}.smooth{:.0f}.fits".format(
                    suffix, scales[i]*3600.
                ),
                hdu[0].data, hdr,
                overwrite=True
            )

        d_convolved_maps.append(d_filtered_c)
        
        if i == 0:
            # Convolve the current open map 3 times the original filter
            # prior to running the second filter to help reduce artefacts.
            final_beam = get_beam(convolving_beams[i]*3)
            convolving_beam = deconvolve_beam(final_beam, beam)

            data = convolve(
                arr=d_filtered,
                pixscale=cd,
                major=convolving_beam.major.value,
                minor=convolving_beam.minor.value,
                pa=convolving_beam.pa.value,
                maxsize=max_size,
                parallel=parallel
            )

            if output_all:
                hdu[0].data = assign_data(hdu, data)
                fits.writeto(
                    image.replace(".fits", "") + "{}.smooth{:.0f}.fits".format(
                        suffix, scales[0]*3600*3.
                    ),
                    hdu[0].data, hdr,
                    overwrite=True
                )

    logger.debug("Subtracting second open map from first open map.")
    combined_filtered = d_convolved_maps[0] - d_convolved_maps[1]

    logger.debug("Resetting zero level in the final open map")
    combined_filtered = reset_zero_level(combined_filtered, clip=True)




    hdu[0].data = assign_data(hdu, combined_filtered)
    fits.writeto(
        image.replace(".fits", "") + "{}.diffuse.fits".format(suffix), 
        hdu[0].data, 
        hdr, 
        overwrite=True
    )

    if output_all:

        # Output scales < filtering scale 1:)
        writeout_compact_map(
            data=np.squeeze(fits.getdata(image)),
            open_map=d_convolved_maps[0],
            hdu=hdu,
            outname=image.replace(".fits", "") + "{}.compact.fits".format(suffix)
        )

        # Output scales < filtering scale 1 or > filtering scale 2:
        writeout_compact_map(
            data=np.squeeze(fits.getdata(image)),
            open_map=combined_filtered,
            hdu=hdu,
            outname=image.replace(".fits", "") + "{}.residual.fits".format(suffix)
        )

        # Output angular scales < filtering scale 2:
        writeout_compact_map(
            data=np.squeeze(fits.getdata(image)),
            open_map=d_convolved_maps[1],
            hdu=hdu,
            outname=image.replace(".fits", "") + "{}.nolargescale.fits".format(suffix)
        )



    logger.debug("Finished.")



