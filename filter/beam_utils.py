#!/usr/bin/env python

"""
Python implementation of the diffuse image filtering technique described
by L. Rudnick (2002):
https://ui.adsabs.harvard.edu/abs/2002NewAR..46..101R/abstract

Implemented in python by S. Duchesne.
Modifications by L. Rudnick. Default options:
Open map 3x beam - open map 27xbeam, zero, convolve 3xbeam.

Options allow general modification of the above procedure. 
"""


import warnings
from astropy.convolution import convolve_fft, Gaussian2DKernel
from astropy import units as u
import numpy as np

from scipy.fft import fftn, ifftn

import logging
logger = logging.getLogger("filter.logger")

try:
    from tqdm.contrib import itertools
except ImportError:
    logger.warning("No tqdm package, will use normal itertools (noprogress bars!)")
    import itertools

from radio_beam import Beam
from radio_beam.utils import BeamError


def fwhm_to_sigma(fwhm):
    """Convert FWHM to sigma.

    Args:
        fwhm: FWHM.

    Returns:
        Sigma from the given FWHM in the same units.

    """

    return fwhm/np.sqrt(8.*np.log(2.))


def get_beam_from_header(header):
    """Wrapper for obtaining beam information from radio-beam."""
    beam = Beam.from_fits_header(header)
    logger.info("Beam: {:.1f}\" * {:.1f}\", {:.1f} deg".format(
        beam.major.value*3600., beam.minor.value*3600., beam.pa.value
    ))
    return beam


def get_beam(bmaj, bmin=None, bpa=0):
    """Wrapper for obtaining a Beam object."""
    if bmin is None:
        bmin = bmaj
    beam = Beam(
        major=bmaj*u.deg,
        minor=bmin*u.deg,
        pa=bpa*u.deg
    )
    return beam


def deconvolve_beam(big_beam, small_beam):
    """Wrapper for beam deconvolution."""
    
    try:
        convolving_beam = big_beam.deconvolve(small_beam)
    except BeamError:
        logger.warning("Beam could not be deconvolved, assuming input beam.")
        convolving_beam = big_beam

    logger.debug(
        "Convolving beam: {0:.1f}\" * {1:.1f}\", {2:.1f} deg".format(
            convolving_beam.major.value*3600.,
            convolving_beam.minor.value*3600., 
            convolving_beam.pa.value
        )
    )

    return convolving_beam


def fft_multi(array):
    """Wrapper for FFT function allowing parallelisation."""
    return fftn(array, workers=-1)


def ifft_multi(array):
    """Wrapper for iFFT function allowing parallelisation."""
    return ifftn(array, workers=-1)


def convolve(arr, pixscale, major, minor, pa,
    maxsize=1024,
    border=72,
    parallel=False):
    """Convolve an array.

    Args:
        arr:
            Array to convolve.
        pixscale:
            Angular size of the pixels.
        major:
            Major axis for the convolving kernel.
        minor:
            Minor axis for the convolving kernel.
        pa:
            Position angle for the convolving kernel.
        maxsize:
            Maximum subarray size for convolution. Convolving a whole image 
            using `convolve_fft` is memory intensive, so chunking is typically
            done. If `maxsize` is greater than the image size,
            the whole image is convolved at once. Default 1024 pixels.
        border:
            Border to pad convolution for subarrays. Avoids aliasing
            problems. Default 100 pixels.
        parallel:
            Switch on parallelisation in the iFFT/FFT routines. Default is to
            not use parallelisation and when enabled ALL cores will be used.

    Returns:
        Convolved array.

    """

    sigma_x = fwhm_to_sigma(major/pixscale)
    sigma_y = fwhm_to_sigma(minor/pixscale)
    theta = np.radians(pa)

    kern = Gaussian2DKernel(sigma_x, sigma_y, theta)

    do_chunks = (np.max(arr.shape) > maxsize) and (maxsize > 0)

    # Edge effects occur if the boundaries are too close to the beam size - 
    # extra padding is somewhat arbitrary at the moment, but a factor of 5 
    # seems to work well.
    if (maxsize < sigma_x*5 or maxsize < sigma_y*5) and do_chunks:
        
        maxsize = int(max([sigma_x, sigma_y])*2)
        logger.warning(
            "Max chunk size smaller than convolving beam - "
            " increasing max chunk size to {}".format(maxsize)
        )
    
    if (border < sigma_x*5 or maxsize < sigma_y*5) and do_chunks:
        
        border = int(max([sigma_x, sigma_y])*5)
        logger.warning(
            "Convolution border size is smaller than convolving beam - "
            " increasing border size to {}".format(border)
        )

    if border > maxsize and do_chunks:
        
        logger.info("Setting max chunk size to match border size.")
        maxsize = border
    
    if parallel:
        logger.debug("Using parallel iFFT/FFT routines.")
        fft_func = fft_multi
        ifft_func = ifft_multi
    else:
        fft_func = fftn
        ifft_func = ifftn

    if do_chunks:
        # Convolve in chunks across the image to preserve memory:
        # Determine number of subarrays:
        logger.warning(
            "Array larger than max convolution size - processing array in chunks."
        )
        nx = arr.shape[-2] // maxsize
        ny = arr.shape[-1] // maxsize
        rx = arr.shape[-2] % maxsize
        ry = arr.shape[-1] % maxsize

        if rx > 0:
            nx += 1
        if ry > 0:
            ny += 1

        garr = np.squeeze(arr)
        conv_arr = np.full_like(garr, np.nan)
        garr_indices = np.indices(garr.shape)

        n = 0
        x_range = range(0, garr.shape[-2], maxsize)
        y_range = range(0, garr.shape[-1], maxsize)

        for i, j in itertools.product(x_range, y_range):

            subx = garr_indices[0][i:i+maxsize, j:j+maxsize]
            suby = garr_indices[1][i:i+maxsize, j:j+maxsize]
    
            if i-border < 0:
                i_min_border = 0
            else:
                i_min_border = border
            if i+maxsize+border > garr.shape[-2]-1:
                i_max_border = 0
            else:
                i_max_border = border
            if j-border < 0:
                j_min_border = 0
            else:
                j_min_border = border
            if j+maxsize+border > garr.shape[-1]-1:
                j_max_border = 0
            else:
                j_max_border = border

            sub_garr = garr[
                i-i_min_border:i+maxsize+i_max_border, 
                j-j_min_border:j+maxsize+j_max_border
            ]

            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                conv = convolve_fft(sub_garr, kern,
                    allow_huge=True,
                    boundary="fill",
                    fill_value=np.nan,
                    normalize_kernel=True,
                    preserve_nan=True,
                    fftn=fft_func,
                    ifftn=ifft_func
                )

            conv = conv[
                i_min_border:sub_garr.shape[-2]-i_max_border, 
                j_min_border:sub_garr.shape[-1]-j_max_border
            ]
            conv_arr[subx, suby]  = conv

            n += 1

    else:

        conv_arr = convolve_fft(
            arr,
            kern,
            preserve_nan=True,
            fill_value=0.,
            allow_huge=True,
            normalize_kernel=True,
            fftn=fft_func,
            ifftn=ifft_func
        ).astype(np.single)

    return conv_arr