# DiffuseFilter

Filter out emission of a given scale via the minimum/maxmimum filtering method described by [Rudnick (2002)](https://ui.adsabs.harvard.edu/abs/2002NewAR..46..101R/abstract). 

CURRENTLY A WORK IN PROGRESS - expect random changes at any time! Documentation will be updated at the GitLab Wiki pages: [https://gitlab.com/Sunmish/diffusefilter/-/wikis/home](https://gitlab.com/Sunmish/diffusefilter/-/wikis/home).

Generally one should be cautious when measuring flux densities from these maps: it is important to ensure the zero level is set correctly for the location of interest, and to note what angular scales have been filtered out of the image.

 
## Installation
Installation can be done in the usual way, e.g.
```
git clone https://gitlab.com/Sunmish/diffusefilter.git
cd diffusefilter
pip install .
```

## Requirements
The `python` requirements are `astropy`, `numpy`, `scipy`, and `radio-beam`. These are set as requirements in the `setup.py` script and should be installed automatically if installing as per the above instructions. Additionally, `tqdm` should be installed if you want to have progress bar output for the convolution step.

## Usage
EMU-specific filtering pipeline:
```
usage: EMU_filter.py [-h] [-S SUFFIX] [-O] [-d] [-c CLIP] [-m MAX_SIZE] [-p] image

Pipeline for EMU extra-galactic field filtering.

positional arguments:
  image                 The image to filter.

options:
  -h, --help            show this help message and exit
  -S SUFFIX, -o SUFFIX, --suffix SUFFIX, --outbase SUFFIX
                        Suffix to append to output image names. 
                        Default is to append only ".diffuse.fits" to the input image name. 
                        If the output image exists it WILL be overwritten.
  -O, --output_all      Output all intermediate images. 
                        A default naming scheme will be used along with --suffix.
  -d, --debug           Enable debug output.
  -c CLIP, --clip CLIP  Sigma clip level: negative values below the sigma clipping 
                        thresholds will be clipped prior to filtering. 
                        Sensible values range from 2-5 sigma. Default 4.
  -m MAX_SIZE, --max_size MAX_SIZE
                        Maximum image size for convolution before breaking it into chunks 
                        to reduce RAM footprint. If working on a high-memory machine (>=64 GB), 
                        this number can  be arbitrarily high. Default -1 (no chunks).
  --no_parallel         Disable parallelisation of the convolving routine. 
                        All cores are normally used, but disable if encountering issues.
```


Generic filtering pipeline:
```
usage: generic_filter.py [-h] [-s [SCALE ...]] [-f [FILTER_FACTOR ...]] [-c CLIP_LEVEL] [-R] [-r] [-M MAX_SIZE]
                         [-C {final,after,before}] [-b [BEAM_SCALE ...]] [-B [BEAM_FILTER_FACTORS ...]] [-FB BEAM_FINAL] [-S SUFFIX]
                         [-O] [-d]
                         image

positional arguments:
  image

options:
  -h, --help            show this help message and exit
  -s [SCALE ...], --scale [SCALE ...]
                        Scale for diffuse filtering in arcsec. 
                        Default None (use --filter_factor instead).
  -f [FILTER_FACTOR ...], --filter_factor [FILTER_FACTOR ...]
                        Factor times the PSF major axis to filter emission. 
                        If multiple factors are supplied, multiple rounds of 
                        filtering are done. Default [3]. Not used if --scale is set.
  -c CLIP_LEVEL, --clip_level CLIP_LEVEL
                        Map values below -1*clip_level will be clipped. 
                        Default is to clip based on 1*rms.
  -R, --use_rms_for_clip
                        Use the map rms for calculating the clip level. 
                        The clip level is then clip_level*rms.
  -r, --replace_clip    Replace clipped pixels with clip value.
  -M MAX_SIZE, --max_size MAX_SIZE
                        Maximum image size for convolution before breaking it 
                        into chunks (saves RAM!). Default 1024.
  -C {final,after,before}, --convolve_setting {final,after,before}
                        Convolving option. Either as a final step ("final"), after
                        ALL filtering ("after"), or before EACH subsequent filter ("before").
  -b [BEAM_SCALE ...], --beam_scale [BEAM_SCALE ...]
                        Smooth output data to this resolution (arcsec). 
                        Default is to use the input filtering scales depending on --convolve_setting.
  -B [BEAM_FILTER_FACTORS ...], --beam_filter_factors [BEAM_FILTER_FACTORS ...]
  -FB BEAM_FINAL, --beam_final BEAM_FINAL
  -S SUFFIX, -o SUFFIX, --suffix SUFFIX, --outbase SUFFIX
                        Suffix to append to output image names. Default is to append
                         only ".diffuse.fits" to the input image name. If the output 
                         image exists it WILL be overwritten.
  -O, --output_all      Output all intermediate images. A default naming scheme will be used.
  -d, --debug           Enable debug statements.
```

